﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;

namespace Darts.Controls
{
    public class CheckedButton : Button
    {
        #region Constructors
        public CheckedButton(Context context) : base(context)
        {
        }

        public CheckedButton(Context context, IAttributeSet attrs) : base(context, attrs)
        {
            Initialize();
        }

        public CheckedButton(Context context, IAttributeSet attrs, int defStyleAttr) : base(context, attrs, defStyleAttr)
        {
        }

        public CheckedButton(Context context, IAttributeSet attrs, int defStyleAttr, int defStyleRes) : base(context, attrs, defStyleAttr, defStyleRes)
        {
        }

        protected CheckedButton(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer)
        {
        }
        #endregion
        private bool _checked;

        public bool Checked
        {
            get { return _checked; }
            set
            {
                _checked = value;
                SetBackgroundColor(value);
            }
        }

        public Android.Graphics.Color CheckedColor = Android.Graphics.Color.Rgb(66, 134, 244);
        public Android.Graphics.Color UncheckedColor = Android.Graphics.Color.Rgb(182, 187, 196);

        private void SetBackgroundColor(bool _checked)
        {
            var buttonColor = _checked ? CheckedColor : UncheckedColor;
            var textColor = !_checked ? CheckedColor : UncheckedColor;
            this.SetBackgroundColor(buttonColor);
            this.SetTextColor(textColor);
        }

        private void Initialize()
        {
            this.Click += CheckedButton_Click;

            Checked = false;
        }

        private void CheckedButton_Click(object sender, EventArgs e)
        {
            Checked = !Checked;
        }
    }
}