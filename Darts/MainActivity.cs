﻿using Android.App;
using Android.Widget;
using Android.OS;
using System.Linq;
using System.Collections.Generic;
using System;
using Darts.Activities;
using Darts.Models;
using Darts.Helpers;
using Newtonsoft.Json;

namespace Darts
{
    [Activity(Label = "Darts 301 Scoreboard", MainLauncher = true)]
    public class MainActivity : Activity
    {
        List<EditText> all = new List<EditText>();
        Spinner spinner;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            
            SetContentView(Resource.Layout.Main);
            Initialize();
            SetupSpinner();
            SetupStartButton();
        }

        private void SetupStartButton()
        {
            var button = FindViewById<Button>(Resource.Id.startButton);
            button.Click += Button_Click;
        }

        private void Button_Click(object sender, EventArgs e)
        {
            if (spinner.SelectedItemPosition == 0)
            {
                Toast.MakeText(this, "You must add at least one player", ToastLength.Long).Show();
                return;
            }
            var names = GetPlayerNames(spinner.SelectedItemPosition);
            List<User> users = new List<User>();
            foreach (var name in names)
            {
                var user = new User()
                {
                    Name = name,
                    RemainingPoints = Constants.Points_301
                };
                users.Add(user);
            }
            Android.Content.Intent intent = new Android.Content.Intent(this, typeof(GameScreenActivity));
            intent.PutExtra(Constants.USER_KEY, JsonConvert.SerializeObject(users));
            this.StartActivity(intent);
            this.Finish();
        }

        private void Initialize()
        {
            all.Add(FindViewById<EditText>(Resource.Id.PlayerName1));
            all.Add(FindViewById<EditText>(Resource.Id.PlayerName2));
            all.Add(FindViewById<EditText>(Resource.Id.PlayerName3));
            all.Add(FindViewById<EditText>(Resource.Id.PlayerName4));
            all.Add(FindViewById<EditText>(Resource.Id.PlayerName5));
            all.Add(FindViewById<EditText>(Resource.Id.PlayerName6));
            all.Add(FindViewById<EditText>(Resource.Id.PlayerName7));
            all.Add(FindViewById<EditText>(Resource.Id.PlayerName8));
        }

        private void SetupSpinner()
        {
            spinner = FindViewById<Spinner>(Resource.Id.playersSpinner);
            var items = Enumerable.Range(0, 8).ToList();
            var adapter = new ArrayAdapter<int>(this, Android.Resource.Layout.SimpleSpinnerItem, items);
            spinner.Adapter = adapter;
            spinner.ItemSelected += Spinner_ItemSelected;
        }

        private void Spinner_ItemSelected(object sender, AdapterView.ItemSelectedEventArgs e)
        {
            var num = e.Position;
            ShowPlayers(num);
        }

        private List<string> GetPlayerNames(int count)
        {
            var result = new List<string>();
            for (int i = 0; i < count; i++)
            {
                string name = string.IsNullOrEmpty(all[i].Text) ? all[i].Hint : all[i].Text;
                result.Add(name);
            }
            return result;
        }

        private void ShowPlayers(int num)
        {
            HideAll();

            for (int i = 0; i < num; i++)
            {
                all[i].Visibility = Android.Views.ViewStates.Visible;
            }
        }

        private void HideAll()
        {
            foreach (var editText in all)
            {
                editText.Visibility = Android.Views.ViewStates.Gone;
            }
        }
    }
}

