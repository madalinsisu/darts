﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Darts.Models;
using Newtonsoft.Json;
using Darts.Helpers;
using Darts.Controls;

namespace Darts.Activities
{
    [Activity(Label = "Darts 301 Scoreboard")]
    public class GameScreenActivity : Activity
    {
        #region Private fields
        List<User> users;
        private List<CheckedButton> oneTwentyButtons = new List<CheckedButton>();
        private CheckedButton twentyFiveButton;
        private CheckedButton zeroButton;
        private List<CheckedButton> multiplierButtons = new List<CheckedButton>();
        private int currentNumber;
        private int currentPlayerIndex;
        private int currentThrowIndex;
        #endregion

        #region Overrides
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.GameScreen);
            SetupControls();

            users = JsonConvert.DeserializeObject<List<User>>(Intent.GetStringExtra(Constants.USER_KEY));
            ShowPlayerScoreAndName();
        }
        #endregion

        #region Private methods
        private void SetupControls()
        {
            var resources = new List<int>()
            {
                Resource.Id.button1,
                Resource.Id.button2,
                Resource.Id.button3,
                Resource.Id.button4,
                Resource.Id.button5,
                Resource.Id.button6,
                Resource.Id.button7,
                Resource.Id.button8,
                Resource.Id.button9,
                Resource.Id.button10,
                Resource.Id.button11,
                Resource.Id.button12,
                Resource.Id.button13,
                Resource.Id.button14,
                Resource.Id.button15,
                Resource.Id.button16,
                Resource.Id.button17,
                Resource.Id.button18,
                Resource.Id.button19,
                Resource.Id.button20,
            };
            foreach (var resId in resources)
            {
                var checkedButton = FindViewById<CheckedButton>(resId);
                oneTwentyButtons.Add(checkedButton);
            }
            resources = new List<int>()
            {
                Resource.Id.buttonX1,
                Resource.Id.buttonX2,
                Resource.Id.buttonX3
            };
            foreach (var resId in resources)
            {
                var checkedButton = FindViewById<CheckedButton>(resId);
                multiplierButtons.Add(checkedButton);
            }
            twentyFiveButton = FindViewById<CheckedButton>(Resource.Id.button25);
            zeroButton = FindViewById<CheckedButton>(Resource.Id.button0);

            foreach (var button in oneTwentyButtons)
            {
                button.Click += Button_Click;
            }
            twentyFiveButton.Click += Button_Click;
            zeroButton.Click += Button_Click;
            foreach (var multiplierButton in multiplierButtons)
            {
                multiplierButton.Click += MultiplierButton_Click;
            }

            var submitButton = FindViewById<Button>(Resource.Id.buttonSubmit);
            submitButton.Click += SubmitButton_Click;
        }

        private void ShowAllMutipliers()
        {
            foreach (var multiplierButton in multiplierButtons)
            {
                multiplierButton.Visibility = ViewStates.Visible;
            }
        }

        private void UncheckMultipliers()
        {
            foreach (var multiplierButton in multiplierButtons)
            {
                multiplierButton.Checked = false;
            }
        }

        private void UncheckNumbers()
        {
            foreach (var button in oneTwentyButtons)
            {
                button.Checked = false;
            }
            twentyFiveButton.Checked = false;
            zeroButton.Checked = false;
        }

        private void UncheckAll()
        {
            UncheckMultipliers();
            UncheckNumbers();
        }

        private int MultiplierNumber()
        {
            for (int i = 0; i < multiplierButtons.Count; i++)
            {
                if (multiplierButtons[i].Checked)
                {
                    return i + 1;
                }
            }
            return 1;
        }

        private int NextPlayerIndex()
        {
            return (currentPlayerIndex + 1) % users.Count;
        }

        private void ShowWinner()
        {
            var user = users[currentPlayerIndex];
            Toast.MakeText(this, $"{user.Name} wins \nCongratulations!", ToastLength.Long).Show();
        }

        private void ShowPlayerScoreAndName()
        {
            var user = users[currentPlayerIndex];
            var lblName = FindViewById<TextView>(Resource.Id.lblCurrentPlayer);
            var lblPoints = FindViewById<TextView>(Resource.Id.lblPoints);
            lblName.Text = user.Name;
            lblPoints.Text = user.RemainingPoints.ToString();
        }
        #endregion

        #region Event handlers
        private void SubmitButton_Click(object sender, EventArgs e)
        {
            var totalCurrentThrow = currentNumber * MultiplierNumber();
            var currentUser = users[currentPlayerIndex];
            if(currentUser.RemainingPoints >= totalCurrentThrow)
            {
                currentUser.RemainingPoints -= totalCurrentThrow;
                if(currentUser.RemainingPoints == 0)
                {
                    ShowWinner();
                    return;
                }
            }
            else
            {
                //Message: too much points
                return;
            }
            ShowPlayerScoreAndName();
            if(currentThrowIndex == 2)
            {
                currentThrowIndex = 0;
                currentPlayerIndex = NextPlayerIndex();
                System.Threading.Tasks.Task.Delay(1000);
                ShowPlayerScoreAndName();
            }
            else
            {
                currentThrowIndex++;
            }
            
            UncheckAll();
        }
        
        private void MultiplierButton_Click(object sender, EventArgs e)
        {
            UncheckMultipliers();
            (sender as CheckedButton).Checked = true;
        }

        private void Button_Click(object sender, EventArgs e)
        {
            UncheckNumbers();
            twentyFiveButton.Checked = false;
            zeroButton.Checked = false;
            (sender as CheckedButton).Checked = true;
            ShowAllMutipliers();
            if (sender == twentyFiveButton)
            {
                multiplierButtons[2].Visibility = ViewStates.Invisible;
            }
            else if(sender == zeroButton)
            {
                foreach (var multiplierButton in multiplierButtons)
                {
                    multiplierButton.Visibility = ViewStates.Invisible;
                }
            }
            currentNumber = int.Parse((sender as CheckedButton).Text);
        }
        #endregion
    }
}